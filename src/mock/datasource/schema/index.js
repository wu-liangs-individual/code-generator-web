import mock from 'mockjs'

mock.mock('/api/datasource/datasourceSchema/findPageByKeywords', require('./response/listPage.json'))

mock.mock('/api/datasource/datasourceSchema/save', require('./response/save.json'))

mock.mock(RegExp('/api/datasource/datasourceSchema/deleteOne/' + '\\d*'), require('./response/delete.json'))
