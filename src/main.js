import Vue from 'vue'
import App from './App.vue'
import elementUI from 'element-ui'
import router from './router'
import 'element-ui/lib/theme-chalk/index.css'
import request from "@/utils/request";

require('./mock/index')

Vue.config.productionTip = false
Vue.prototype.$request = request

Vue.use(elementUI)

new Vue({
    render: h => h(App),
    router
}).$mount('#app')
