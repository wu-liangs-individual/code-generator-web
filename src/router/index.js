import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)


export default new Router({
// export default router = new Router({
    mode: "history",
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: '/',
            name: 'index',
            meta: {title: '主页', noCache: true},
            component: (resolve) => require(['@/views/index'], resolve),
            children: [
                {
                    path: '/custom/cardList',
                    name: 'cardList',
                    component: (resolve) => require(['@/components/custom/cardList/index.vue'], resolve),
                },
                {
                    path: '/custom/patient',
                    name: 'patient',
                    component: (resolve) => require(['@/components/custom/patient/index.vue'], resolve),
                },
                {
                    path: '/custom/processor',
                    name: 'processor',
                    component: (resolve) => require(['@/components/custom/processor/index.vue'], resolve),
                },
                {
                    path: '/custom/tab',
                    name: 'tab',
                    component: (resolve) => require(['@/components/custom/tab/index.vue'], resolve),
                },
                {
                    path: '/custom/timeList',
                    name: 'timeList',
                    component: (resolve) => require(['@/components/custom/timeLineList/index.vue'], resolve),
                },
                {
                    path: '/custom/toDoList',
                    name: 'toDoList',
                    component: (resolve) => require(['@/components/custom/toDoList/index.vue'], resolve),
                },
                {
                    path: '/custom/userList',
                    name: 'userList',
                    component: (resolve) => require(['@/components/custom/userList/index.vue'], resolve),
                },
            ]
        },
        {
            path: '/login',
            name: 'login',
            meta: {title: '登录', noCache: true},
            component: (resolve) => require(['@/views/login'], resolve),
        },
        {
            path: '/view/components',
            name: 'previewComponents',
            component: resolve => require(['@/views/componentView'], resolve),
        },

    ]
})

// router.beforeEach((to, from, next) => {
// if (!localStorage.getItem("token")) {
//   next({path: '/login'})
// }
// })
