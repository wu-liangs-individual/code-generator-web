import axios from 'axios'
import {Notification} from 'element-ui'

const request = axios.create({
  headers: {
    "token": "",
    "Content-Type": "application/json",
  },
  baseURL: process.env.NODE_ENV == 'pro' ? '' : ''
  // baseURL: process.env.NODE_ENV == 'pro' ? '' : 'http://localhost:9000/'
})

request.interceptors.response.use(response => {
  if (response.status === 200) {
    console.log('接收到响应: ')
    console.log(response.data)
    if (response.data.returnCode === 200) {
      if (response.data.value) {
        return response.data.value;
      } else {
        return response.data;
      }
    } else {
      Notification.error(response.data.message)
    }
  }
})

export default request
