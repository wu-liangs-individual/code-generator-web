module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',
  assetsDir: "./",
  devServer: {
    port: 8999,
    disableHostCheck: true,
    open: true,
    https: false,
    //以上的ip和端口是我们本机的;下面为需要跨域的
    // proxy: {//配置跨域
      // '/api': {
      //   target: 'http://localhost:9000/',//这里后台的地址模拟的;应该填写你们真实的后台接口
      //   ws: true,
      //   changOrigin: true,//允许跨域
      // }
    // }
  }
}
